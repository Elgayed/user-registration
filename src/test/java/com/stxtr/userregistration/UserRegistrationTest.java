package com.stxtr.userregistration;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stxtr.rest.ApiError;


/*
 * Ideally the rest api client library would be implemented or generated (feign), and used to test the REST API,
 * that way an easy way is provided to test both the API contract and the client lib
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserRegistrationTest {
	
    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private ObjectMapper mapper;
    
    private URI userRegistrationEndpointUri;
	
    // created user registration endpoint URI, after local server port is injected
    @PostConstruct
    private void init() {
    	userRegistrationEndpointUri = URI.create(String.format("http://localhost:%d/%s", port, "userservice/register"));
    }
    
    @Test
    public void testRegisterUser() throws JSONException {
    	ResponseEntity<String> response = registerUser(generateUserMap("Foo", "Bar", "foo_bar", "foo_bar_password"));
        
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        JSONObject respJsonBody = new JSONObject(response.getBody());
        assertNotNull(respJsonBody.get("id"));
        assertNull(respJsonBody.opt("password"));
        assertEquals("Foo", respJsonBody.get("firstName"));
        assertEquals("Bar", respJsonBody.get("lastName"));
        assertEquals("foo_bar", respJsonBody.get("userName"));
        assertNull(respJsonBody.opt("password"));
    }
    
    @Test
    public void testRegisterDuplicateUser() throws JsonMappingException, JsonProcessingException {
    	ResponseEntity<String> firstRegistration = registerUser(generateUserMap("John", "Doe", "john_doe", "john_doe_password"));
        
        assertEquals(HttpStatus.CREATED, firstRegistration.getStatusCode());
        
        ResponseEntity<String> secondRegistration = registerUser(generateUserMap("John 1", "Doe 1", "john_doe", "john_doe_password 1"));
        
        
        assertEquals(HttpStatus.CONFLICT, secondRegistration.getStatusCode());
        String respBody = secondRegistration.getBody();
        ApiError error =  mapper.readValue(respBody, ApiError.class);
        assertEquals("USER_ALREADY_EXISTS", error.getCode());
        assertEquals("A user with the given username already exists", error.getDescription());
    }
    
    @Test
    public void testRegisterUserWithBlankFirstName() {
    	ResponseEntity<String> response = registerUser( generateUserMap(null, "Last Name 1", "User Name 1", "Password 1"));
    	assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }
    
    @Test
    public void testRegisterUserWithBlankLastName() {
    	ResponseEntity<String> response = registerUser( generateUserMap("First Name 2", "", "User Name 2", "Password 2"));
    	assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }
    
    @Test
    public void testRegisterUserWithBlankUserName() {
    	ResponseEntity<String> response = registerUser( generateUserMap("First Name 3", "Last Name 3", null, "Password 3"));
    	assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }
    
    @Test
    public void testRegisterUserWithBlankPassword() {
    	ResponseEntity<String> response = registerUser( generateUserMap("First Name 4", "Last Name 4", "User Name 4", null));
    	assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    private ResponseEntity<String> registerUser (Map<String, String> userToRegister) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	return restTemplate.exchange(userRegistrationEndpointUri, HttpMethod.POST, new HttpEntity<Map<String, String>>(userToRegister, headers), String.class);
    }
    
    private static Map<String, String> generateUserMap (final String firstName, final String lastName, final String userName, final String password) {
    	Map<String, String> userMap = new HashMap<>();
    	userMap.put("firstName", firstName);
    	userMap.put("lastName", lastName);
    	userMap.put("userName", userName);
    	userMap.put("password", password);
    	return userMap;
    } 
}
