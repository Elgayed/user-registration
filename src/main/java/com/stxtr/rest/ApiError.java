package com.stxtr.rest;

public class ApiError {

	private String code;
	private String description;
	
	public ApiError() {}
	
	public ApiError(String code, String description) {
		super();
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}
}
