package com.stxtr.service;

import com.stxtr.exceptions.UserAlreadyExistsException;
import com.stxtr.model.UserDto;

public interface UserRegistrationService {
	
	public UserDto createUser (final UserDto userDto) throws UserAlreadyExistsException ;
}
