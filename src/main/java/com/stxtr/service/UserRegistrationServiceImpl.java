package com.stxtr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.stxtr.exceptions.UserAlreadyExistsException;
import com.stxtr.model.UserDto;
import com.stxtr.userrepository.User;
import com.stxtr.userrepository.UserRepository;

@Service
public class UserRegistrationServiceImpl implements UserRegistrationService {
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PasswordEncoder encoder;
	
	@Override
	public UserDto createUser(UserDto userDto) throws UserAlreadyExistsException {
		User user = new User();
		user.setUserName(userDto.getUserName());
		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		String plainPassword = userDto.getPassword();
		user.setPlainTextPassword(plainPassword);
		user.setHashedPassword(encoder.encode(plainPassword));
		User createdUser = userRepository.createUser(user);
		userDto.setId(createdUser.getId());
		return userDto;
	}

}
