package com.stxtr.userrepository;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Repository;

import com.stxtr.exceptions.UserAlreadyExistsException;

@Repository
public class UserRepositoryImpl implements UserRepository {
	
	private ConcurrentHashMap<String,User> usersStore = new ConcurrentHashMap<>();
	
	@Override
	public User createUser(User user) throws UserAlreadyExistsException {
		if (usersStore.containsKey(user.getUserName()))
			throw new UserAlreadyExistsException(String.format("User with username '%s' already exists", user.getUserName()));
		
		user.setId(UUID.randomUUID().toString());
		usersStore.put(user.getUserName(), user);
		return user;
	}

}
