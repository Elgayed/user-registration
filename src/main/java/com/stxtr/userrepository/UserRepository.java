package com.stxtr.userrepository;

import com.stxtr.exceptions.UserAlreadyExistsException;

public interface UserRepository {
	
	public User createUser( User user ) throws UserAlreadyExistsException;
}
 
  
 
